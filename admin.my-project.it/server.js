var express = require('express'),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    session = require('cookie-session'),
    auth = require('basic-auth'),
    path = require('path'),
    swig = require('swig'),
    fs = require('fs'),
    config = require('../config');

var app = express(),
    nodeEnv = process.env.NODE_ENV || "development";

app.use('/assets', express.static(__dirname + config[nodeEnv].app_path + '/assets'));

app.set('port', process.env.PORT || 3000);

app.use(bodyParser());

// Set express' views location
app.set('views', __dirname + '/public/app');

app.engine('html', swig.renderFile);

// Swig will cache templates for you, but you can disable
// that and use Express's caching instead, if you like:
app.set('view cache', false);// To disable Swig's cache, do the following:
swig.setDefaults({ cache: false });

// Session management
app.use(session({
    secret: 'soigthoiwegnw48ehg08gowhno9gn480n',
    cookie: {
        secure: true,
        maxAge: 1000 * 60 * 60 * 24 * 30 // 30 days
    }
}));

// dynamically include routes (Controller)
fs.readdirSync(__dirname + '/controllers').forEach(function (file) {
    if(file.substr(-3) == '.js') {
        var controller = require('./controllers/' + file);
        controller(app, nodeEnv);
    }
});

module.exports = app;

// Comment the next two lines if you are using express' vhosts
app.listen(app.get('port'));
app.use(morgan('dev'));
console.log('Express server listening on port ' + app.get('port') + ' in ' + nodeEnv + ' environment');