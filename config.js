var config = {
    projectName: 'my-project.it',
    dbName: 'my-project',

    development: {
        vhost: {
            api: 'api.my-project.dev',
            admin: 'admin.my-project.dev',
            www: 'www.my-project.dev'
        },
        app_auth: {
            username: 'user',
            password: 'pwd'
        },
        api_auth: {
            username: 'api_user',
            password: 'api_pwd'
        },
        app_path: '/public/app'
    },

    production :{
        vhost: {
            api: 'api.my-project.it',
            admin: 'admin.my-project.it',
            www: 'www.my-project.it'
        },
        app_auth: {
            username: 'user',
            password: 'pwd'
        },
        api_auth: {
            username: 'api_user',
            password: 'api_pwd'
        },
        app_path: '/public/dist'
    }
};

module.exports = config;