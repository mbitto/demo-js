var express = require('express'),
    morgan = require('morgan'),
    vhost = require('vhost'),
    errorHandler = require('errorhandler'),
    compress = require('compression'),
    config = require('./config'),
    apiServer = require('./api.' + config.projectName + '/server'),
    adminServer = require('./admin.' + config.projectName + '/server'),
    wwwServer = require('./www.' + config.projectName + '/server');

var app = express(),
    nodeEnv = process.env.NODE_ENV || "development";

app.set('port', process.env.PORT || 3000);

// Logger
app.use(morgan(':method :url -- :referrer (:date)'));

// vhosts
app.use(vhost(config[nodeEnv].vhost.api, apiServer));
app.use(vhost(config[nodeEnv].vhost.admin, adminServer));
app.use(vhost(config[nodeEnv].vhost.www, wwwServer));

// Configure dev stuff here
if (nodeEnv === 'development') {
    app.use(errorHandler({ dumpExceptions: true, showStack: true }));
}

app.listen(app.get('port'));

console.log('Express server listening on port ' + app.get('port') + ' in ' + nodeEnv + ' environment');