var express = require('express'),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    session = require('cookie-session'),
    auth = require('basic-auth'),
    path = require('path'),
    fs = require('fs'),
    api = require('./helpers/api.js'),
    config = require('../config');

var app = express(),
    nodeEnv = process.env.NODE_ENV || "development";

app.set('port', process.env.PORT || 3000);

app.use(bodyParser());


// Session management
app.use(session({
    secret: 'soigthoiwegnw48ehg08gowhno9gn480n',
    cookie: {
        secure: true,
        maxAge: 1000 * 60 * 60 * 24 * 30 // 30 days
    }
}));


// API authorization custom middleware
var apiAuth = function(req, res, next){
    var user = auth(req);

    if (user &&
        user.name === config[nodeEnv].api_auth.username &&
        user.pass === config[nodeEnv].api_auth.password) {
        next();
    } else {
        res.statusCode = 401;
        res.setHeader('WWW-Authenticate', 'Basic realm="Authorization required"');
        res.end('Unauthorized');
    }
};


// CORS middleware
app.all('*', function(req, res, next){
    if (!req.get('Origin')) return next();
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
    if ('OPTIONS' == req.method) return res.send(200);
    next();
});


// Routes for news
app.get('/news', api.news.findAll);
app.post('/news', apiAuth, api.news.create);
app.get('/news/:id', api.news.findOne);
app.put('/news/:id', apiAuth, api.news.update);
app.delete('/news/:id', apiAuth, api.news.delete);


module.exports = app;

// Comment the next lines if you are using express' vhosts
app.use(morgan('dev'));
app.listen(app.get('port'));
console.log('Express server listening on port ' + app.get('port') + ' in ' + nodeEnv + ' environment');