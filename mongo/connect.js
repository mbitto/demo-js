var dbName = "my-project",
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    fs = require('fs'),
    mongoClient = new MongoClient(new Server('localhost', 27017)),
    dbConnection = null;

module.exports = function(collection, callback){
    if(dbConnection === null){
        mongoClient.connect("mongodb://localhost:27017/" + dbName, function(err, db) {
            if(err) console.error(err);
            dbConnection = db;
            callback(db.collection(collection));
        });
    }
    else{
        callback(dbConnection.collection(collection));
    }
};