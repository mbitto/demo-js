var express = require('express'),
    check = require('validator').check,
    sanitize = require('validator').sanitize,
    path = require('path'),
    morgan = require('morgan'),
    swig = require('swig'),
    swigExtras = require('swig-extras'),
    fs = require('fs'),
    config = require('../config');

var app = express(),
    nodeEnv = process.env.NODE_ENV || "development";

app.set('port', process.env.PORT || 3000);

swigExtras.useFilter(swig, 'truncate');

// We don't mind about express.static because nginx will serve all static files for us

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

// Swig will cache templates for you, but you can disable
// that and use Express's caching instead, if you like:
app.set('view cache', false);// To disable Swig's cache, do the following:
swig.setDefaults({ cache: false });


// Dynamically include routes
fs.readdirSync(__dirname + '/controllers').forEach(function (file) {
    if(file.substr(-3) == '.js') {
        var controller = require('./controllers/' + file);
        controller(app);
    }
});

// Redirect on home page not matching routes
app.get('/*', function(req, res){
    res.redirect(301, '/');
});

module.exports = app;

// Comment the next lines if you are using express' vhosts
app.use(morgan('dev'));
app.listen(app.get('port'));
console.log('Express server listening on port ' + app.get('port') + ' in ' + nodeEnv + ' environment');